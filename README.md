# About Us!

Welcome to the Clarkston High School Coding Club </CHS_CODING_CLUB>
This is a friendly environment where will go places into code where normal academic classes would not go in-depth about
We'll talk about design and the process of building your own website, games, and anything else you'd want to, while actually building them.

Unlike an academic class, this will further your knowledge into coding and help you design and create things online that you have never done or considered doing by yourself based of school knowledge yourself!

With this year-long club, we aim to help you increase your chances of landing a job in Computer Science! Even if you're not going for a computer science path way, this will help you in the long run with many jobs as things transfer over to the technology side of the world. Coding is an amazing skillset to have to prove yourself to employers that you have a wide range of skill that can be applied anywhere.

From competitions to internships, we hope to fulfill those dreams.

# Schedule

We plan to meet every Tuesday starting March 28th, 2023.

# Restrictions

## DISCORD SECTION
1. No NSFW images or videos, obviously. There’s teachers and adults here, bozo.

2. Be respectful, don’t be a butthole. Discrimination is a big no no.

3. Don’t be a pest, keep your annoying-ism to a low level.

4. Keep profanity and other inappropriate content to a low level.

5. Don’t spread hatred, misinformation, or rumors about anyone, we want to keep this place safe and positive.

6. Importantly, have common sense. You all are highschoolers, don’t frick it up.

7. All criticism should be constructive, not destructive.

8. Follow Discord TOS, (terms of service), because if you’re not in the Discord, how will you do anything?...

## CLUB SECTION
1. Try your best to attend your meetings although, it won’t matter too much, there is content you should know about.

2. All club members are welcome, but we do hold the right to restrict you from coming to our meetings for a reason that we find fit.

3. Keep everything clean, your workstations and area.

4. Try to keep your code completely original and from scratch, although looking up snippets and using their code in your own way is completely allowed, but make sure you understand how that code works. I’ll be watching…

5. Do not mess with other peoples code unless you are 100% sure they are completely fine with it. Code takes a long time to write, butthole.

6. Treat your equipment with respect.

7. Make sure your commits and pushes have a descriptive name and description.

# Events

About events like TEXD, State Conferences @ Athens, Internships, Guest Speakers, Visitors from Major & Minor Companies, it will all be announced and pinged. It will also be sent to the button at the top of the channels list that says, believe it or not, Events.

If you are interested, it is preferred that you press the “Interested” button on the Event card so we can keep track of who is coming.

Votes will also be created to see what you guys want to do.

# Afterword

All in all, this club is completely for fun and educational purposes. We want everyone to be in a safe environment to learn and have fun coding We want to help expose the tech industry to student to have a head start in the industry.

We will try to continue to have events and meetings (hopefully!) long after the presidents have graduated. Make the most of it, learn the most you can. We plan to lay the foundation for this club and hopes to give this club the right direction it needs to be successful.

Thank you for your prolonged interest in the Coding Club!
